# Analizador de Archivos - Seguridad - ASO 2020 - UTN-FRC
![](Imágen.png)

El analizador de archivos es un proyecto para la materia Arquitectura de Software, que registra todos los ejecutables que están corriendo en una máquina y realiza un análisis de seguridad sobre los mismos 

## Enunciado:
### Seguridad Analizador de Archivos - 
#### Historia y Requerimientos
Un emprendimiento de la ciudad de Córdoba, busca ganar un lugar en el saturado mercado
de la seguridad informática.
Aunque no se cuenta aún con la capacidad de mano de obra para generar un antivirus
propio, se cuenta con la ayuda de https://www.virustotal.com/es/, capaz de recibir archivos,
analizarlos y brindar un reporte de seguridad del mismo.
Se quiere crear entonces, una aplicación que se pueda instalar en una máquina de usuario
(independiente del sistema operativo) y permita seleccionar un archivo para ser enviado a
virus total y recibir el correspondiente reporte de análisis.
Por el momento, sólo los archivos PE (Portable-Executable) deberían poder ser enviados.

#### La primera versión ha sido un éxito the Mood - Glenn Miller,
por lo cual se ha decido buscar la manera de estar adelantados a la competencia. Se ha
pensado en que los reportes que se reciben de VirusTotal deberían guardarse en una BD
local, de manera tal que no se pregunte más de una vez por un archivo determinado.
Teniendo una BD local con esta información, el siguiente paso es poder compartir esa
información con otros clientes, de manera tal que ya no se trate de un programa
independiente, sino que se pueda consultar de alguna manera antes de pedir un informe a
VirusTotal.
La aplicación que brinda este servicio debería poder recibir entre 8 y 10 peticiones por
minuto, pero no se descarta que la aplicación pueda servir a un volúmen mayor.
#### Se reciben fondos de financiamiento
Esto hará que la empresa pueda crecer y distribuir el producto en una mayor escala,
llegando a otros tipos de clientes corporativos.
Sin embargo, se ha cambiado el enfoque del producto. Partiendo de la base de que
tenemos una BD con reportes de archivos, los vendedores convencieron a la gerencia de
que el producto se dedique a realizar análisis sobre el comportamiento de los diferentes
puntos de acceso de una compañía.
Entonces será necesario contar con un mecanismo que registre en nuestro servidor todos
los ejecutables que un grupo de máquinas ejecutan durante un periodo de tiempo.
Con estos ejecutables, el sistema deberá ser capaz de actualizar su base con la información
de VirusTotal que no se posea o no sea mayor a 2 semanas sin actualización.
El objetivo final es que toda esta información se encuentre detallada en nuestra aplicación
para poder realizar un posterior análisis usando Big Data.
Se deberán establecer entonces, mecanismos para:
• Capturar la información de cada archivo en ejecución en cada máquina (endpoint)
• Verificar si se cuenta con los reportes para cada archivo
• Si no se cuenta con el reporte o se ha superado las 2 semanas de TTL (time to live),
se debe pedir el reporte a VirusTotal
• Se deberá acomodar el sistema a los nuevos RNF (req no funcionales) de
escalabilidad, ya que se estima que un usuario ejecuta entre 10-20 procesos por
hora y la cantidad de endpoints no será mínima a 200.
1. Deberán entregar:
2. Código fuente versionado en GitHub
3. Documentación de Arquitectura
a. Definición de Requerimientos y Supuestos
b. Definición de Requerimientos no funcionales
c. Diagrama de Arquitectura
d. Dependencias

## Acerca De:
El analizador de archivos es un proyecto académico. No será mantenido a partir del 26/06/2020

## Diagrama de Arquitectura
![Imágen no encontrada](Diagrama de Arquitectura.png)
Diagrama que representa como el proyecto está estructurado

## Getting Started

Este repositorio está conectado a [otro](https://gitlab.com/aso_2020/nuevo_backend/) que contiene el componente de BackEnd.
Clone ambos repositorios
``` 
git clone https://gitlab.com/aso_2020/nuevo_backend/
git clone https://gitlab.com/aso_2020/analizador_de_arachivos/
```

### Prerrequisitos

Es necesario tener [docker](https://docs.docker.com/get-docker/) instalado para poder utilizar el programa. 


### Instalación
Para usarlo primero se debe iniciar el contenedor 

Construir imagen:

``` 
docker build -t analizador_archivos_be 
```
Correr contenedor:

```
docker container run --detach -p 80:80 analizador_archivos_be
```

## Pruebas

El proyecto estaba pensado para soportar una carga de 200 usuarios que corren de diez a veinte procesos por hora.
Se pensaba utilizar la herramienta Apache JMeter para realizar las pruebas, pero quedaron fuera del alcance de este proyecto.


## Utilización

Esto se tiene que completar

## Construido con:

* Node.js ^12.16.2 LTS
* dotenv ^8.2
* request-promise ^4.2
* fs ^1.0
* mongooose ^5.9.18


## Autores

* **Rouaux, Matías** - *Desarrollo BackEnd* - [@mrouaux10](https://gitlab.com/mrouaux10)
* **Casares Díaz, Mauricio** - *Desarrollo BackEnd* 
* **García Cowan, Eliana Belén** - *Desarrollo FrontEnd* [@elianabgarcia96](https://gitlab.com/elianabgarcia96) 
* **Sosa, Joaquín** - *Documentación* - [@joako93](https://gitlab.com/joako93)



