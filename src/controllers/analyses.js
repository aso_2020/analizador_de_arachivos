'use strict';
const ipc = require('electron').ipcRenderer;
const buttonCreated = document.getElementById('upload');

const fs = require('fs');

const request = require('request-promise');

buttonCreated.addEventListener('click', function (event) {
  ipc.send('open-file-dialog-for-file')
});

ipc.on('selected-file', async function (event, path) {
  let options = getFileOptions(path);

  request(options).then(async (response) => {
    let fileId = JSON.parse(response).data.id;

    let options = getAnalyseOptions(fileId);

    request(options).then((response) => {
      console.log('respuesta final tiene: ', response);
    }).catch(err => {
      console.log(err);
    });
  }).catch(err => {
    console.log(err);
  });
});

const getFileOptions = (path) => {
  return ({
    'method': 'POST',
    'url': 'https://www.virustotal.com/api/v3/files',
    'headers': {
      'x-apikey': process.env.XAPIKEY
    },
    formData: {
      'file': {
        'value': fs.createReadStream(path[0]),
        'options': {
          'filename': 'software1.png',
          'contentType': null
        }
      }
    }
  });
}

const getAnalyseOptions = (fileId) => {
  return {
    'method': 'GET',
    'url': `https://www.virustotal.com/api/v3/analyses/${fileId}`,
    'headers': {
      'x-apikey': process.env.XAPIKEY
    }
  };
}

/* ESTE ES EL CODIGO CON AXIOS POR SI ALGUNO QUERE INTENTAR HACERLO FUNCIONAR
WITH AXIOS

  'use strict';
  const ipc = require('electron').ipcRenderer;
  const buttonCreated = document.getElementById('upload');

  const axios = require('axios');
  const virustotalInstance = axios.create({
    baseURL: 'https://www.virustotal.com/api/v3/',
    headers: { 'x-apikey': process.env.XAPIKEY }
  });
  const fs = require('fs');

buttonCreated.addEventListener('click', function (event) {
  ipc.send('open-file-dialog-for-file')
});

ipc.on('selected-file', function (event, path) {
  let formdata = new FormData();

  formdata.set('filename', 'fileToAnalyse');
  formdata.append('file', fs.createReadStream(path[0]));

  console.log(JSON.stringify(formdata.get('file')));

  virustotalInstance.post('files', {
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    data: formdata,
    responseType: 'json'
  }).then((response) => {
    let fileId = JSON.parse(response.body).data.id;

    virustotalInstance.get(`analyses/${fileId}`, {

    }).then((response) => {
      console.log('respuesta final tiene: ', response.body);
    }).catch(err => {
      handleAxiosErrors(err);
    });
  }).catch(err => {
    handleAxiosErrors(err);
  });
});


const handleAxiosErrors = ( error ) => {
    if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log( 'ERROR RESPONSE DATA ', error.response.data );
        console.log( 'ERROR RESPONSE STATUS ', error.response.status );
        console.log( 'ERROR RESPONSE HEADERS ', error.response.headers );
    } else if ( error.request ) {
        // The request was made but no response was received
        console.log( error.request );
    } else {
        // Something happened in setting up the request that triggered an Error
        console.log( 'Error', error.message );
    }
}


*/