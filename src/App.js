import React, { useState } from "react";
import axios from "axios";
import "./App.css";

async function sendFile(file) {
  const bodyFormData = new FormData();
  bodyFormData.append("archivo", file);

  await axios
    .post("http://localhost:4000/api/analyze/", bodyFormData, {
      headers: { "Content-Type": "multipart/form-data" },
    })
    .then((response) => {
      console.log("TODO OK");
      console.log(response);
    })
    .catch((e) => {
      console.log("TODO MAL");
      console.log(e);
    });
}

function App() {
  const [file, setfile] = useState(null);

  return (
    <div class="container">
      <div class="file-container">
        <div class="file-wrapper">
          <input
            class="file-input"
            id="js-file-input"
            type="file"
            onChange={(e) => setfile(e.target.files[0])}
            name="archivo"
          />
          <div class="file-content">
            <div class="file-infos">
              <div
                class="file-icon"
                style={{
                  display: "flex",
                  justifyContent: "space-around",
                  alignItems: "center",
                  flexDirection: "column",
                }}
              >
                <img
                  alt="upload"
                  width="100px"
                  height="100px"
                  src="https://cdn1.iconfinder.com/data/icons/hawcons/32/698648-icon-130-cloud-upload-128.png"
                ></img>
                <span>Hacer click para elegir un archivo o soltar</span>
              </div>
            </div>
            <div className="file-footer">
              <p class="file-name" id="js-file-name">
                {file ? file.name : "Ningún archivo cargado"}
              </p>
            </div>
          </div>
        </div>
        {file ? (
          <div className="button-container">
            <button className="upload-button" onClick={() => sendFile(file)}>
              Cargar
            </button>
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default App;
