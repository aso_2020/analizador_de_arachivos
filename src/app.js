'use strict';
require('dotenv').config();
var app = require('electron').app;
var ipc = require('electron').ipcMain;
var { dialog } = require('electron');
var os = require('os');

var BrowserWindow = require('electron').BrowserWindow;
var mainWindow = null;

app.on('ready', function () {
    mainWindow = new BrowserWindow({
        resizable: true,
        height: 600,
        width: 800,
        webPreferences: {
            nodeIntegration: true
        }
    });

    mainWindow.loadURL('file://' + __dirname + '/public/index.html');
    mainWindow.on('closed', function () {
        mainWindow = null;
    });
});

ipc.on('open-file-dialog-for-file', function (event) {
    if (os.platform() === 'linux' || os.platform() === 'win32') {
        console.log('ubuntu/win');
        dialog.showOpenDialog({ properties: ['openFile'] })
            .then(file => {
                if (file) event.sender.send('selected-file', file.filePaths);
            })
    } else {
        console.log('mac');
        dialog.showOpenDialog({ properties: ['openFile', 'openDirectory'] })
            .then(file => {
                if (file) event.sender.send('selected-file', file[0]);
            })
    }
});

ipc.on('close-main-window', function () {
    app.quit();
});


